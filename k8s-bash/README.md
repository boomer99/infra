# k8s-cluster
1. set root password
2. switch root account
3. kubeadm init --apiserver-advertise-address 192.168.0.144 --pod-network-cidr=172.16.0.0/16
4.1 flannel network kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml 
OR
4.2 calico best https://projectcalico.docs.tigera.io/getting-started/kubernetes/quickstart (# This section includes base Calico installation configuration.:

cat <<EOF >> calico.yml
# This section includes base Calico installation configuration.
# For more information, see: https://projectcalico.docs.tigera.io/v3.22/reference/installation/api#operator.tigera.io/v1.Installation
apiVersion: operator.tigera.io/v1
kind: Installation
metadata:
  name: default
spec:
  # Configures Calico networking.
  calicoNetwork:
    # Note: The ipPools section cannot be modified post-install.
    ipPools:
    - blockSize: 26
      cidr: 172.16.0.0/16
      encapsulation: VXLANCrossSubnet
      natOutgoing: Enabled
      nodeSelector: all()

---

# This section configures the Calico API server.
# For more information, see: https://projectcalico.docs.tigera.io/v3.22/reference/installation/api#operator.tigera.io/v1.APIServer
apiVersion: operator.tigera.io/v1
kind: APIServer 
metadata: 
  name: default 
spec: {}
EOF


-------------

5.Если проблема с kubelet:
првоерь cgrops