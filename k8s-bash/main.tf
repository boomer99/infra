terraform {
  required_providers {
    virtualbox = {
      source = "terra-farm/virtualbox"
      version = "0.2.2-alpha.1"
    }
  }
}

# There are currently no configuration options for the provider itself.

resource "virtualbox_vm" "node" {
  count     = 1
  name      = "master"
  image     = "https://app.vagrantup.com/ubuntu/boxes/focal64/versions/20220324.0.0/providers/virtualbox.box"
  cpus      = 2
  memory    = "2048 mib"
  #user_data = file("${path.module}/user_data")

  network_adapter {
    type           = "nat"
  }
}

output "IPAddr" {
  value = element(virtualbox_vm.node.*.network_adapter.0.ipv4_address, 1)
}

output "IPAddr_2" {
  value = element(virtualbox_vm.node.*.network_adapter.0.ipv4_address, 2)
}